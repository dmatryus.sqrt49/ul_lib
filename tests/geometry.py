from pathlib import Path

import pytest
import sys

sys.path.append(str(Path(__file__).absolute().parents[1]))

from ul_lib.geometry import Point, PointSet, Rangemeter


@pytest.fixture(scope="function")
def geometry_2d_data():
    p = Point([1, -2])
    ps = PointSet([p, p + 1, p * 2, p * 3, p * -3, p - 7])
    simplex = ps.circumscribe_simplex()
    return {"point": p, "point_set": ps, "simplex": simplex}


@pytest.fixture(scope="function")
def geometry_4d_data():
    p = Point([1, -2, -1, 4])
    ps = PointSet([p, p + 1, p * 2, p * 3, p * -3, p - 7])
    simplex = ps.circumscribe_simplex()
    return {"point": p, "point_set": ps, "simplex": simplex}


# @pytest.mark.parametrize("d", [(geometry_2d_data), (geometry_4d_data)])
def test_simplex(geometry_4d_data):
    ps = geometry_4d_data["point_set"]
    simplex = geometry_4d_data["simplex"]
    print()
    print("test_simplex")
    print("point_set:\n", ps)
    print("simplex:\n", simplex)
    assert ps.dimension == simplex.dimension
    assert len(simplex) == simplex.dimension + 1


def test_distance(geometry_2d_data, geometry_4d_data):
    rm = Rangemeter(func="squared")
    ps = geometry_2d_data["point_set"]
    simplex = geometry_2d_data["simplex"]
    print()
    print("test_distance")
    print("point_set 2d:\n", ps)
    print("simplex 2d:\n", simplex)
    distance = rm.range(ps[0], ps[1])
    print("distance 2d:\n", distance)
    distances = rm.range_matrix(simplex, ps)
    print("distances 2d:\n", distances)
    assert len(distances) == len(simplex)
    assert len(distances[0]) == len(ps)

    ps = geometry_4d_data["point_set"]
    simplex = geometry_4d_data["simplex"]
    print()
    print("point_set 4d:\n", ps)
    print("simplex 4d:\n", simplex)
    distances = rm.range_matrix(simplex, ps)
    print("distances 4d:\n", distances)
    assert len(distances) == len(simplex)
    assert len(distances[0]) == len(ps)
