# UL_lib

A library for easy use of unsupervised learning algorithms. It contains both classic solutions and little-known ones.